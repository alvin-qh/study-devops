package alvin.docker.testing.builder;

public interface ModelBuilder<T> {
    T build();
}
