package alvin.docker.infra.model;

import java.io.Serializable;

public interface Identity extends Serializable {
    Long getId();
}
