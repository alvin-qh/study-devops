# Setup

## Install jupyter bash extension

### Install pip package

```bash
$ pip install bash_kernel
```

### Build and install extension

```bash
$ python -m bash_kernel.install
```