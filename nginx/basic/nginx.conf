
worker_processes      auto;  # or number of cup cores
worker_rlimit_nofile  3000;

events {
    worker_connections  1024;
}

http {
    include       ../common/mime.types;
    default_type  application/octet-stream;
    sendfile      on;
    server_tokens off;
    tcp_nopush    on;
    tcp_nodelay   off;

    client_max_body_size    50m;
    client_body_buffer_size 256k;
    client_header_timeout   3m;
    client_body_timeout     3m;
    send_timeout            3m;

    keepalive_timeout  65;

    # Define a format of log file named 'name'
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    # Record access log in file './logs/access.log' with format main
    access_log  logs/access.log  main;

    upstream backend {
        server localhost:3000;
    }

    # Define a virtual server host
    server {
        # Port to listen
        listen         8091;

        # Name of host to match
#       server_name    *.alvin.com;

        # Default charset
        charset        utf-8;

        # Path of document
        root           www;

        # Location rule
        # Math all URI that start with '/'
        location / {
            # Default content type
            default_type text/html;

            # Default index file
            index index.html;

            # Test if URI matches given regexp, and proxy to different target
            # eg: /routing                        => http://localhost:3000/routing
            #     /routing/question?callback=.... => http://localhost:3000/routing/question?callback=....
            if ($request_uri ~ "^/(\w+)(/.*)?$") {
                proxy_pass http://localhost:3000/$1$2/;
            }
        }

        # Location rule
        # Math all URI that start with '/hello'
        location ^~ /hello {
            default_type application/json;

            # Response
            return 200 '{"status": "OK", "message": "Hello World"}';
        }

        # Location rule
        # Math all URI that start with '/baidu'
        # eg: /baidu?wd=nginx   => https://www.baidu.com/s?wd=nginx
        location ^~ /baidu {
            return 302 https://www.baidu.com/s?$args;
        }

        location ^~ /d {
            proxy_pass         http://backend/;
            proxy_set_header   Host             $host;
            proxy_set_header   X-Real-IP        $remote_addr;
            proxy_set_header   X-Forwarded-For  $proxy_add_x_forwarded_for;
        }

        # Location rule
        # Math all URI that start with '/proxy'
        location ^~ /proxy {
            # proxy to http://localhost:3000
            proxy_pass  http://localhost:3000/;
        }

        location ^~ /css/ {
            proxy_pass  http://localhost:3000/css/;
        }

        location ^~ /js/ {
            proxy_pass  http://localhost:3000/js/;
        }
    }
}
