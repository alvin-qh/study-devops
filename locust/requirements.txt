certifi==2018.11.29
chardet==3.0.4
Click==7.0
dnspython==1.16.0
eventlet==0.24.1
Flask==1.0.2
gevent==1.4.0
greenlet==0.4.15
gunicorn==19.9.0
idna==2.8
itsdangerous==1.1.0
Jinja2==2.10.1
locustio==0.9.0
MarkupSafe==1.1.0
monotonic==1.5
msgpack==0.6.0
pyzmq==17.1.2
requests==2.21.0
six==1.12.0
urllib3==1.24.1
Werkzeug==0.14.1
WTForms==2.2.1
